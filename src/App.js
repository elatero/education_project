import React from 'react'
import Layout from './Layout/containers/LayoutContainer'

function App() {
  return (
    <div className='App'>
      <Layout />
    </div>
  )
}

export default App
