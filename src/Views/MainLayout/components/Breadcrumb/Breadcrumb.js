import React from 'react'
import { Grid, Link, Breadcrumbs } from '@material-ui/core'
import styles from './Breadcrumb.module.sass'

const Breadcrumb = () => {
  return (
    <Grid item xs={12} className={styles.breadcrumb}>
      <Breadcrumbs aria-label='breadcrumb' className={styles.breadcrumbNav}>
        <Link color='inherit' href='/'>
          Главная страница
        </Link>
      </Breadcrumbs>
    </Grid>
  )
}

export default Breadcrumb
