import React from 'react'
import { Card, Avatar, CardContent, Typography, Link } from '@material-ui/core'
import styles from './Profile.module.sass'

const Profile = () => {
  return (
    <Card className={styles.container}>
      <CardContent className={styles.cardHeader}>
        <Typography align='center' variant='h5'>
          Ваш аккаунт
        </Typography>
        <Avatar className={styles.avatar}>ИИ</Avatar>
        <Typography
          align='center'
          color='primary'
          variant='h5'
          className={styles.name}
        >
          Иван Иванов
        </Typography>
        <Typography color='textPrimary' variant='subtitle1'>
          Статус: Студент
        </Typography>
        <Typography color='textPrimary' variant='subtitle1'>
          Номер группы: 1
        </Typography>
        <Link href='##' className={styles.edit}>
          Редактировать профиль
        </Link>
      </CardContent>
    </Card>
  )
}

export default Profile
