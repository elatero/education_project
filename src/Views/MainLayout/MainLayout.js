import React from 'react'
import { Link } from 'react-router-dom'
import { Grid, Typography, MenuList, Paper } from '@material-ui/core'
import styles from './MainLayout.module.sass'
import Breadcrumb from './components/Breadcrumb'
import Profile from './components/Profile/index'
import RouterMainLayout from './../../Route/RouteMainLayout'

const MainPage = (props) => {
  return (
    <Grid container className={styles.MainPage}>
      <Grid item sm={2} className={styles.box}>
        <Profile />
      </Grid>
      <Grid item sm={8}>
        <Breadcrumb />
        <RouterMainLayout />
      </Grid>
      <Grid item sm={2} className={styles.box}>
        <Typography align='center' color='primary' variant='h5'>
          Навигация:
        </Typography>
        <Typography align='center' variant='h6'>
          <Paper>
            <MenuList>
              <li>
                <Link to='schedule'>Рассписание</Link>
              </li>
              <li>
                <Link to='/'>На главную</Link>
              </li>
            </MenuList>
          </Paper>
        </Typography>
      </Grid>
    </Grid>
  )
}

export default MainPage
