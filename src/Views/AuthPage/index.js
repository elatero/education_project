import * as actions from './actions'
import * as actionTypes from './actionTypes'
import components from './components'
import containers from './containers'
import reducer from './reducer'
import * as selectors from './selectors'

export default {
  actions,
  actionTypes,
  components,
  containers,
  reducer,
  selectors
}
