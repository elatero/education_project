import * as t from './actionTypes'
import { fromJS } from 'immutable'

const initialState = fromJS({
  isReg: false
})

const authPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case t.IS_REGISTRATION:
      return state.set('isReg', !state.get('isReg'))

    default:
      return state
  }
}

export default authPageReducer
