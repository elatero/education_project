import { connect } from 'react-redux'
import { isRegistrationSelector } from '../../selectors'
import { isRegistration } from '../../actions'

import AuthPage from '../../components/AuthPage'

const mapStateToProps = (state, props) => ({
  isReg: isRegistrationSelector(state)
})

const mapDispatchToProps = {
  isRegistration
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthPage)
