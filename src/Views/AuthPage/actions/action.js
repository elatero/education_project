import { IS_REGISTRATION } from '../actionTypes'

export const isRegistration = (props) => async (dispatch) => {
  dispatch({
    type: IS_REGISTRATION
  })
}
