import { createSelector } from 'reselect'

const getAuthSelector = (store) => store.auth

export const isRegistrationSelector = createSelector(getAuthSelector, (state) =>
  state.get('isReg')
)

export const isAuthSelector = createSelector(getAuthSelector, (state) =>
  state.get('isAuth')
)
