// @flow

import * as React from 'react'
import { Grid, FormControlLabel, Switch } from '@material-ui/core'
import styles from './AuthPage.module.sass'
import AuthForm from '../AuthForm'
import RegForm from '../RegForm'

type TProps = {
  isReg: boolean,
  isRegistration: () => Promise<void>
}

const AuthPages = (props: TProps) => {
  const formAuth = props.isReg ? <RegForm /> : <AuthForm />

  console.log(props)

  return (
    <Grid
      container
      direction='column'
      alignItems='center'
      className={styles.container}
    >
      <h1 className={styles.title}>Добро пожаловать!</h1>
      <p className={styles.description}>
        Образовательный ресурс для студентов!
      </p>
      <div className={styles.switch}>
        <FormControlLabel
          control={
            <Switch
              checked={props.isReg}
              onChange={() => {
                props.isRegistration()
              }}
              value={props.isReg}
              color='primary'
            />
          }
          labelPlacement='start'
          label='Зарегестрироваться'
          name='switchRegistration'
        />
      </div>
      {formAuth}
    </Grid>
  )
}

export default AuthPages
