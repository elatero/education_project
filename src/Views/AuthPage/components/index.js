import AuthForm from './RegForm'
import AuthPage from './AuthPage'

export default {
  AuthPage,
  AuthForm
}
