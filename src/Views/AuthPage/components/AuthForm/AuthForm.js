import React from 'react'
import { TextField, Button, Link } from '@material-ui/core'
import styles from './AuthForm.module.sass'

const AuthForm = () => {
  return (
    <form className={styles.form} noValidate autoComplete='off'>
      <div>
        <TextField
          fullWidth
          id='name'
          name='name'
          label='Имя пользователя'
          type='text'
          autoComplete='on'
          variant='outlined'
          placeholder='Введите имя пользователя'
          margin='normal'
        />
        <TextField
          fullWidth
          id='password'
          name='password'
          label='Пароль'
          type='password'
          autoComplete='off'
          variant='outlined'
          placeholder='Введите пароль'
          margin='normal'
        />
        <div className={styles.boxBtn}>
          <Button
            variant='contained'
            color='primary'
            size='medium'
            className={styles.button}
          >
            Войти
          </Button>
          <Link href='##'>Забыли пароль?</Link>
        </div>
      </div>
    </form>
  )
}

export default AuthForm
