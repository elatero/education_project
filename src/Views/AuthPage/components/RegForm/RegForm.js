import React from 'react'
import { TextField, Button } from '@material-ui/core'
import styles from './RegForm.module.sass'

const RegForm = () => {
  return (
    <form className={styles.form} noValidate autoComplete='off'>
      <div>
        <TextField
          fullWidth
          id='name'
          name='name'
          label='Имя пользователя'
          type='text'
          autoComplete='on'
          variant='outlined'
          placeholder='Введите имя пользователя'
          margin='normal'
        />
        <TextField
          fullWidth
          id='mail'
          name='mail'
          label='Адрес электронной почты'
          type='mail'
          autoComplete='on'
          variant='outlined'
          placeholder='Введите адрес электронной почты'
          margin='normal'
        />
        <TextField
          fullWidth
          id='password'
          name='password'
          label='Пароль'
          type='password'
          autoComplete='off'
          variant='outlined'
          placeholder='Введите пароль'
          margin='normal'
        />
        <TextField
          fullWidth
          id='confirm'
          name='confirm'
          label='Повторите пароль'
          type='password'
          autoComplete='off'
          variant='outlined'
          placeholder='Введите пароль повторно'
          margin='normal'
        />
        <Button
          variant='contained'
          color='primary'
          size='medium'
          className={styles.button}
        >
          Зарегестрироваться
        </Button>
      </div>
    </form>
  )
}

export default RegForm
