import React from 'react'
import { Typography } from '@material-ui/core'
import styles from './PostPage.module.sass'
import AddPost from './components/AddPost'
import CardItem from './components/CardItem'

const PostPage = (props) => {
  const isAdd = true

  return (
    <React.Fragment>
      <Typography
        align='center'
        color='primary'
        variant='h4'
        className={styles.title}
      >
        Доска заданий
      </Typography>
      <AddPost isAdd={isAdd} />
      <CardItem />
    </React.Fragment>
  )
}

export default PostPage
