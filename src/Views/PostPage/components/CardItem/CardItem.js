import React from 'react'
import {
  Link,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  IconButton,
  Typography
} from '@material-ui/core'
import FavoriteIcon from '@material-ui/icons/Favorite'
import ShareIcon from '@material-ui/icons/Share'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile'
import styles from './CardItem.module.sass'

const CardItem = () => {
  return (
    <Card className={styles.card}>
      <CardHeader
        action={
          <IconButton aria-label='settings'>
            <MoreVertIcon />
          </IconButton>
        }
        title='Заголовок поста (например: название предмета)'
        subheader='Подзаголовок'
      />
      <CardMedia className={styles.media} image='Images' title='Paella dish' />
      <CardContent className={styles.cardFiles}>
        <InsertDriveFileIcon fontSize='large' htmlColor='#999' />
        <Link href='##' className={styles.link}>
          Link.pdf
        </Link>
      </CardContent>
      <CardContent className={styles.cardContent}>
        <Typography variant='body2' color='textSecondary' component='p'>
          Контрольная работа номера вариантов по последним цифрам зачетки!
        </Typography>
      </CardContent>
      <CardContent className={styles.cardFooter}>
        <CardActions disableSpacing>
          <IconButton aria-label='add to favorites'>
            <FavoriteIcon />
          </IconButton>
          <IconButton aria-label='share'>
            <ShareIcon />
          </IconButton>
        </CardActions>
        <Typography>
          Добавлено:
          <span className={styles.cardDate}>16 января 2020</span>
        </Typography>
      </CardContent>
    </Card>
  )
}

export default CardItem
