// @flow

import * as React from 'react'
import { Fab, TextField, Button } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import styles from './AddPost.module.sass'

type TProps = {
  isAdd: boolean
}

const AddPost = (props: TProps) => {
  const formPost = (
    <form action='/post' method='POST' noValidate className={styles.formAdd}>
      <TextField
        id='title'
        name='title'
        label='Заголовок'
        variant='outlined'
        margin='dense'
        fullWidth
        required
      />
      <TextField
        id='subtitle'
        name='subtitle'
        label='Подзаголовок'
        variant='outlined'
        margin='dense'
        fullWidth
      />
      <textarea
        name='text'
        id='textAdd'
        placeholder='Введите текст'
        wrap='hard'
        rows='5'
        className={styles.textContent}
      ></textarea>
      <input
        className={styles.inputFile}
        type='file'
        id='file'
        name='file'
        accept='.pdf, .doc, image/*'
        multiple
      />
      <Button
        variant='contained'
        color='primary'
        type='submit'
        className={styles.btnAdd}
      >
        добавить пост
      </Button>
    </form>
  )

  return (
    <div className={styles.container}>
      <div className={styles.addBtnBox}>
        <Fab color='primary' aria-label='add'>
          <AddIcon />
        </Fab>
      </div>
      {props.isAdd ? formPost : null}
    </div>
  )
}

export default AddPost
