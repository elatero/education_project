import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
import createRootReducer from './createRootReducer'

const configureStore = () => {
  let store

  if (window.__REDUX_DEVTOOLS_EXTENSION__) {
    store = createStore(
      createRootReducer,
      compose(
        applyMiddleware(thunk),
        window.__REDUX_DEVTOOLS_EXTENSION__ &&
          window.__REDUX_DEVTOOLS_EXTENSION__()
      )
    )
  } else {
    store = createStore(createRootReducer, compose(applyMiddleware(thunk)))
  }

  return store
}

export default configureStore
