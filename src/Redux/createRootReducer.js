import { combineReducers } from 'redux'
import authPageReducer from '../Views/AuthPage/reducer'
import layoutReducer from '../Layout/reducer'

export default combineReducers({
  auth: authPageReducer,
  layout: layoutReducer
})
