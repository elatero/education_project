import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import PostPage from '../../Views/PostPage'
import SchedulePage from './../../Views/SchedulePage'

const RouterMainLayout = () => {
  const router = (
    <Switch>
      <Route exact path='/' component={PostPage} />
      <Route path='/schedule' component={SchedulePage} />
      <Redirect to='/' />
    </Switch>
  )

  return <React.Fragment>{router}</React.Fragment>
}

export default RouterMainLayout
