// @flow

import * as React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import AuthPage from '../../Views/AuthPage'
import MainLayout from '../../Views/MainLayout'

type TProps = {
  isAuth: boolean
}

const RouterLayout = (props: TProps) => {
  let router = (
    <Switch>
      <Redirect exact from='/' to='/auth' />
      <Route path='/auth' component={AuthPage.containers.AuthContainer} />
      <Redirect to='/auth' />
    </Switch>
  )

  if (props.isAuth) {
    router = (
      <Switch>
        <Route component={MainLayout} />
      </Switch>
    )
  }

  return <React.Fragment>{router}</React.Fragment>
}

export default RouterLayout
