import ApolloClient from 'apollo-boost'

const client = new ApolloClient({
  uri: 'http://localhost:5100/api/graphql'
})

export default client
