import React from 'react'
import styles from './Layout.module.sass'
import Logo from '../../../Assets/logo.jpg'
import { Container, Grid, Link } from '@material-ui/core'

const Layout = ({ children }) => {
  return (
    <Container maxWidth={false} className={styles.layout}>
      <Grid
        container
        xs={12}
        justify='center'
        item={true}
        className={styles.header}
      >
        <Link href='/' className={styles.linkLogo}>
          <img src={Logo} alt='Logo' className={styles.logo} />
        </Link>
      </Grid>
      <Grid container xs={12} justify='center' item className={styles.main}>
        {children}
      </Grid>
      <Grid
        container
        xs={12}
        justify='center'
        item={true}
        className={styles.footer}
      >
        <p>Copyright © 2020 "BBS-GROUP"</p>
      </Grid>
    </Container>
  )
}

export default Layout
