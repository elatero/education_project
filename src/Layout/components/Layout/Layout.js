// @flow

import * as React from 'react'
import { Link } from 'react-router-dom'
import styles from './Layout.module.sass'
import Logo from '../../../Assets/logo.jpg'
import { Container, Grid } from '@material-ui/core'
import RouterLayout from '../../../Route/RouteLayout'

type TProps = {
  isAuth: boolean
}

const Layout = ({ isAuth }: TProps) => {
  return (
    <Container maxWidth={false} className={styles.layout}>
      <Grid
        container
        xs={12}
        justify='center'
        item={true}
        className={styles.header}
      >
        <Link to='/' className={styles.linkLogo}>
          <img src={Logo} alt='Logo' className={styles.logo} />
        </Link>
      </Grid>
      <Grid container xs={12} justify='center' item className={styles.main}>
        <RouterLayout isAuth={isAuth} />
      </Grid>
      <Grid
        container
        xs={12}
        justify='center'
        item={true}
        className={styles.footer}
      >
        <p>Copyright © 2020 {' BBS-GROUP '}</p>
      </Grid>
    </Container>
  )
}

export default Layout
