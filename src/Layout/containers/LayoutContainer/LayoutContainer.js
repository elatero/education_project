import { connect } from 'react-redux'
import { isAuthSelector } from '../../selectors'
import { isAuthication } from '../../actions'

import Layout from '../../components/Layout'

const mapStateToProps = (state, props) => ({
  isAuth: isAuthSelector(state)
})

const mapDispatchToProps = {
  isAuthication
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout)
