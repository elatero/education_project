import { createSelector } from 'reselect'

const getAuthSelector = (store) => store.layout

export const isAuthSelector = createSelector(getAuthSelector, (state) =>
  state.get('isAuth')
)
