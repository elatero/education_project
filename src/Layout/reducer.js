import * as t from './actionTypes'
import { fromJS } from 'immutable'

const initialState = fromJS({
  isAuth: true
})

const layoutReducer = (state = initialState, action) => {
  switch (action.type) {
    case t.IS_AUTH:
      return state.set('isAuth', state.get('isAuth'))

    default:
      return state
  }
}

export default layoutReducer
